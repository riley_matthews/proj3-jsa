# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## Authors 

Initial version by M Young; Docker version added by R Durairajan; to be revised by CIS 322 students. 

This version revised by Riley Matthews

## Status

./run.sh to automatically build the docker image and run the container.

Uses the base vocab functionality with form interaction replaced by AJAX interaction on each keystroke.

status messages will be displayed after each keystroke such as:

 - List of found words.
 - New word found.
 - current text is not a word.
 - word has already been found.